(define-situation foo (bar))

(define-rule foo () base () (bar)
  (format t "base says ~a~%" bar)
  (suppress 'squish-me)
  (return-from-rule 7))

(define-rule foo (base) squish-me () (bar)
  (format t "squish-me says ~a; ERROR~%" bar))

(define-rule foo () prebase (base) (bar)
  (format t "prebase says ~a~%" bar))

(define-rule foo (squish-me) abort () (bar) 
  (format t "abort says ~a~%" (evaluated-so-far))
  (abort-rule)
  (format t "This rule should have aborted before now." bar))
