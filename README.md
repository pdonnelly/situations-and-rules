## Situations and Rules

A *situation* is a function whose behavior is defined by a set of *rules* associated with it. Situations and rules address the following scenario: suppose we have a function implementing a basic behavior, but over time we expect to want to add additional behaviors to it, without changing older code in the process. Rules are defined individually with the `DEFINE-RULE` macro, and when their associated situation is called, they are called in a defined order.

## Code Example

    (define-situation who-likes (input))

    (define-rule who-likes () base-rule () (input)
      (when (= input 1) (print 'base-likes-it)))

    (define-rule who-likes () preceding-rule (base-rule) (input)
      (when (= input 0) (print 'preceding-likes-it)))

    (define-rule who-likes (base-rule) following-rule () (input)
      (print 'following-likes-everything))

The output will be:

    PRECEDING-LIKES-IT
    FOLLOWING-LIKES-EVERYTHING

## Motivation

The idea came about in the context of programming a roguelike (forthcoming). A roguelike may develop a maze-like system of rules over the years. To write a game that evolves, it must be made easy to add new game rules without making changes in multiple places.

## Documentation

Additional documentation for the code is in the file [exposition.org](exposition.org).