;; Situations and rules are akin to generic functions in that a situation is called as a function, and when called runs some number of rules. The differences are that a situation will almost always run *multiple* rules, and that as a consequence a situation has no return value.
;; A rule, then, is much like a method, except that rules have unique names, to provide for their distinct features. When a rule is defined, it replaces any rules of that situation which have the same name. It is also given a place in the situation list which is consistent with its declared predecessor and successor rules. When a situation is called, the rules run in the order in which they appear in the list. The point of this is that new rules can be added, and can be given a place in the list, without making changes to old rules. You should be picturing a supplement to a board game, which comes with supplemental rules to the original rule book.
;; Rules are provided with two more facilities that aim to help them meet that goal. A rule can state that some other rules (by name) should be suppressed and that none of their code should run. A rule can inspect the list of rules which have previously run.
;; A note is in order about what it means for a rule to "run". A rule has the third facility that it may "abort" itself (later rules will still get to run). When a rule aborts, it is not considered to have "run" and will not be visible to later rules as having run. The purpose of this is that rules are expected to first check if they "apply" in light of their given arguments and the current world state. A rule which does not "apply" should abort without having had any side effects (honor system), and from its non-presence in the list of rules run, interested later rules can modify their behavior.

(in-package :situations-and-rules)

(defmacro macrowhere (body &rest definitions)
  `(macrolet ,definitions ,body))

(defstruct (situation (:constructor %make-situation)) name-symbol arg-list (rule-list '()))

(defstruct rule name function)

(defvar *situations* (make-hash-table)
  "Hashes situation names (symbols) to situation objects.")

(defun make-situation (name-symbol arg-list)
  (let ((s (%make-situation :name-symbol name-symbol
			    :arg-list arg-list)))
    (setf (gethash name-symbol *situations*) s)
    s))

(defun eval-situation (name-symbol args)
  (macrowhere
   (let* ((evaluated-so-far ()) ; Some rules may want to check the list of rules run and choose not to run.
          (return-value ())
          (return-a-value-p nil)) ; Unless some rule returns a value, we don't want to.
     ;; assume rules are sorted by priority
     (do ((rs (situation-rule-list (gethash name-symbol *situations*)) (cdr rs)))
         ((null rs))
       (|Eval the rule|))
     (if return-a-value-p return-value (values)))
   
   (|Collect return value|
    () `(when retval-p
          (setf return-a-value-p t)
          (push retval return-value)))
   
   (|Eval the rule|
    () `(multiple-value-bind (retval retval-p suppressed |rule was aborted|)
            (apply (rule-function (car rs)) evaluated-so-far args)
          ;;(format t "Rules now: ~a~%" (mapcar #'rule-name rs))
          (unless |rule was aborted|
            (|Collect return value|)
            (push (rule-name (car rs)) evaluated-so-far)
            (when suppressed
              (setf rs (remove-if (lambda (r) (member (rule-name r) suppressed))
                                  rs))))))))

(defmacro define-situation (name-symbol (&rest args))
  "Define a situation called NAME-SYMBOL, which takes arguments ARGS. ARGS must just be a list of arguments, all required (that is, no keyword args or optional args are supported). On subsequent runs, a DEFINE-SITUATION form will wipe the list of rules clean."
  (macrowhere
   (progn
     (|Validate define-situation arguments|)
     (format t "Defining situation ~a.~%" name-symbol)
     (make-situation name-symbol args)
     `(defun ,name-symbol ,args
        (eval-situation ',name-symbol (list ,@args))))
   
   (|Validate define-situation arguments|
    ()
    '(progn
      (check-type name-symbol symbol)
      (dolist (a args) (check-type a symbol))))))

(defun add-rule (situation-name-symbol rule-name-symbol args predecessors successors rule-fn)
  (macrowhere
   (progn
     (|Validate argument types|)
     (let ((s (gethash situation-name-symbol *situations*)))
       (|Be sure that rule and situation match|)
       (format t "Defining rule ~a for situation ~a.~%" rule-name-symbol situation-name-symbol)
       (setf (situation-rule-list s)
             (ordered-list-insert
              (make-rule :name rule-name-symbol :function rule-fn)
              (remove rule-name-symbol (situation-rule-list s)
                      :key #'rule-name)
              predecessors successors
              #'rule-name))))
   
   (|Validate argument types|
    () '(progn
         (check-type situation-name-symbol symbol)
         (check-type rule-name-symbol symbol)
         (dolist (a args) (check-type a symbol))))
   
   (|Be sure that rule and situation match|
    () '(progn
         (assert (not (null s)) ()
          "Unable to look up situation ~a for rule ~a."
          situation-name-symbol rule-name-symbol)
         (assert (= (length args) (length (situation-arg-list s))))))))

(defmacro define-rule (situation-name-symbol (&rest predecessors) rule-name-symbol (&rest successors) (&rest args) &body body)
  "Define a rule for the situation called SITUATION-NAME-SYMBOL. This should be a symbol. This rule will be called after any rule named in PREDECESSORS and before any named in SUCCESSORS. RULE-NAME-SYMBOL will be used by other rules to refer to this one (e.g. in the aforementioned arguments or when suppressing a rule). ARGS must correspond to the args in the DEFINE-SITUATION, although the names may be different.

Within BODY there are some affordances: the macros ABORT-RULE, RETURN-FROM-RULE, SUPPRESS, and EVALUATED-SO-FAR. See [[file:exposition.org::*Features%20of%20situations%20and%20rules][Features of situations and rules]]"
  (let ((suppressed (gensym "SUP"))
        (so-far (gensym "SOF"))
        (block (gensym "B")))
    `(add-rule ',situation-name-symbol
               ',rule-name-symbol
               ',args
               ',predecessors
               ',successors
               (lambda (,so-far ,@args)
                 (declare (ignorable ,so-far))
                 (macrolet ((abort-rule () (list 'return-from ',block '(values nil nil nil t)))
                            (return-from-rule (retval)
                              (list 'return-from ',block (list 'values retval t ',suppressed nil)))
                            (suppress (rule-name-symbol-to-suppress)
                              (list 'push rule-name-symbol-to-suppress ',suppressed))
                            (evaluated-so-far () ',so-far))
                   (block ,block
                     (let ((,suppressed '()))
                       ,@body
                       (return-from ,block (values nil nil ,suppressed nil)))))))))

(defmacro aifnt (it then &body else)
  `(let ((it ,it))
     (if (not it)
         ,then
         (progn ,@else))))

(defmacro |Diagnose insertion error| ()
  '(error
    (cond ((member current-key successors)
           "Cannot insert; ~a appears as a predecessor and as a successor.")
          (t "Cannot insert; the predecessor ~a has followed a successor."))
    current-key))

(defmacro |Find insertion point| ()
  ;; The strategy is to scan the whole list (necessary to be sure the insertion-point is post every predecessor in LIST), noting the cons where a predecessor has most recently been seen. This is the inasertion point, or else it's NIL when there are no predecessors in LIST. That includes the case when LIST is empty.
  '(loop
      with predecessor-means-error = nil
      with insertion-point = nil
      for cons on list
      for current-key = (funcall key (car cons))
      do
        (when (member current-key successors)
          (setf predecessor-means-error t))
        (when (member current-key predecessors)
          (when predecessor-means-error (|Diagnose insertion error|))
          (setf insertion-point cons))
      finally (return insertion-point)))

(defun ordered-list-insert (item list predecessors successors &optional (key #'identity))
  "Insert ITEM into LIST, returning LIST with the item in place. May modify list structure. Signals an error if it detects an ordering conflict in LIST, although no attempt is made to detect theoretically impossible orderings which don't occur in LIST."
  (aifnt (|Find insertion point|)
      (cons item list)
    (rplacd it (cons item (cdr it)))
    list))

(defun list-of-all-situations ()
  (loop for s being the hash-keys in *situations* collect s))
