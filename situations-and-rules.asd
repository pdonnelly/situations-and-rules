(asdf:defsystem #:situations-and-rules
    :description "Rules and situations, akin to functions or methods."
    :author "Paul Donnelly"
    :components ((:file "package")
                 (:file "situations")))
