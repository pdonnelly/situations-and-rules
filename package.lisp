;;;; package.lisp

(defpackage #:situations-and-rules
  (:use #:cl)
  (:export #:define-situation
           #:define-rule
           #:abort-rule
           #:return-from-rule
           #:suppress
           #:evaluated-so-far))

